/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import inicio.Plataforma;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 2
 *
 * @author kevin
 */
public class InterfazPrincipal {

    public Plataforma sistema;
    public Scanner sc;

    public InterfazPrincipal() {
        sistema = new Plataforma();
        sc = new Scanner(System.in);

    }

    public boolean opcionValida(String op) {
        if (op.equals("1") || op.equals("2") || op.equals("3")) {
            return true;
        }
        return false;
    }
    
    public void iniciar() {

        String continuar = "si";

        do {
            System.out.println("MENU PRINCIPAL");
            System.out.println("1. Iniciar Sesion");
            System.out.println("2. Simular");
            System.out.println("3. salir");
            System.out.println("Ingrese su opción:");

            String op = sc.nextLine();

            while (opcionValida(op) == false) {
                System.out.println("Esa opcion no existe, ingrese opcion de nuevo");
                op = sc.nextLine();
            }
            switch (op) {
                case "1":
                    IniciarSesion in = new IniciarSesion(sistema);
                    in.iniciar();
                    break;
                case "2":
                    SimulacionIngresoCiudadela simPrueba = new SimulacionIngresoCiudadela(sistema);
                    simPrueba.menu();
                    break;
                case "3":
                    System.out.println("Sesion finalizada");
                    continuar = "no";
                    break;
            }
            if (continuar.equals("no") == false) {
                System.out.println("Desea volver al menu principal(si/no)");
                continuar = sc.nextLine();
            }

        } while (continuar.equals("si"));

    }

    public static void main(String[] args) {

        InterfazPrincipal IrMain = new InterfazPrincipal();
        IrMain.iniciar();
    }

    

}
