/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import inicio.Casa;
import inicio.Ciudadela;
import inicio.Ingreso;
import inicio.IngresoVehiculo;
import inicio.IngresoVisitante;
import inicio.Mail;

import inicio.Plataforma;
import inicio.Residente;
import inicio.Visitante;
import java.time.LocalDate;
import java.util.Scanner;

/**
 *Class SimulacionIngresoCiudadela
 * Descripcion : Esta clase se la utiliza como una extension de la clase principal
 * mail, con el fin de no generar mucho codigo en una sola clase principal, contiene 
 * sus respectivo constructor, este constructor contiene objetos de tipo plataforma,ciudadela
 * mail and Scanner
 * @author Grupo 7
 */
public class SimulacionIngresoCiudadela {

    public Plataforma sistema;
    public Scanner sc;
    public Ciudadela c;
    public Mail mail;

    public SimulacionIngresoCiudadela(Plataforma s) {
        sistema = s;
        sc = new Scanner(System.in);
        mail = new Mail();
    }

     /**
 * Metodo encontrar ingresoResidente()
 * Descripcion: este le pide al usuario como sera su ingreso, vehiculo o peaton
 * @return No retorna nada, es void
 * @author Grupo 7
 * 
 */
    public void ingresoResidente() {

        System.out.println("Opciones");
        System.out.println("1. Ingreso con vehículo");
        System.out.println("2. Ingreso como peatón");
        //me estoy guiando con lo del documento
        System.out.println("3. Regresar");
        System.out.print("Ingrese su opción:");
        int entrada = sc.nextInt();
        switch (entrada) {
            case 1:
                ingresoVehiculo();
                break;
            case 2:
                ingresoPeaton();
                break;
            case 3:
                sc.nextLine();
                menu();
                break;
            default:
                System.out.println("Opción inválida");
        }

    }
 /**
 * Metodo ingresoVehiculo()
 * Descripcion: Le pide al usuario que ingrese el tipo de matricula
 * y verifica en el sistema si esta matricua existe, en caso de existir si procede a 
 * obtener el residente y accederle el ingreso
 * @author Grupo 7
 * 
 */
    
    public void ingresoVehiculo() {
        //Las cámaras en los brazos robóticos leerán el número de matrícula del vehículo
        long TInicio = System.currentTimeMillis();
        System.out.println("Ingrese número de matrícula");
        sc.nextLine();
        String num = sc.nextLine();

        if (c.encontrarResidentePorVehiculo(num) != null) {
            Residente r = c.encontrarResidentePorVehiculo(num);

            System.out.println("Acceso Concedido");
            long Tfin = System.currentTimeMillis();
            long tiempo = Tfin - TInicio;
            tiempo = tiempo / 1000;
            IngresoVehiculo i = new IngresoVehiculo(LocalDate.now(), "residente", r.getNombreResidente(), r.getCasa().getMz(), r.getCasa().getVilla(), tiempo, num);
            sistema.encontrarCiudadela(c.getNombreCiudadela()).añadirIngreso(i);
        } else {
            System.out.println("Acceso Denegado");
        }

        /*Si el número de matrícula leído corresponde al número de matrícula 
         de alguno de los vehículos registrados el sistema el brazo procederá a autorizar el ingreso*/
        //se llama a algún método que busca al residente por el número de matrícula de su carro/
    }
 /**
 * Metodo ingresoVehiculo()
 * Descripcion: El sistema pedirá el ingreso del número de cédula y pin de acceso de cuatro dígitos del residente.
 * @author Grupo 7
 * 
 */
    public void ingresoPeaton() {
        //El sistema pedirá el ingreso del número de cédula y pin de acceso de cuatro dígitos del residente.
        long TInicio = System.currentTimeMillis();
        System.out.println("Ingrese el número de cédula");
        sc.nextLine();
        String ci = sc.nextLine();
        System.out.println("Ingrese el pin de acceso");
        //sc.nextLine();
        String pin = sc.nextLine();

        if (c.encontrarResidente(pin, ci) != null) {
            Residente r = c.encontrarResidente(pin, ci);
            System.out.println("Acceso Concedido");
            long Tfin = System.currentTimeMillis();
            long tiempo = Tfin - TInicio;
            tiempo = tiempo / 1000;
            Ingreso i = new Ingreso(LocalDate.now(), "residente", r.getNombreResidente(), r.getCasa().getMz(), r.getCasa().getVilla(), tiempo);
            sistema.encontrarCiudadela(c.getNombreCiudadela()).añadirIngreso(i);
        } else {
            System.out.println("Acceso Denegado");
        }
    }
 /**
 * Metodo menu()
 * Descripcion: En este metodo se verifica si la ciudadela ingresa por el usuario existe
 * si existe en esta se anexan los otros metodos de ingresoResidente e ingresoVisitante
 * @author Grupo 7
 * 
 */

    public void menu() {
        System.out.println("Bienvenido");
        int n = 0;

        System.out.println("Ingrese Ciudadela:");
        String ciud = sc.nextLine();
        while (sistema.encontrarCiudadela(ciud) == null) {
            System.out.println("No existe esa ciudadela, Ingrese Ciudadela:");
            ciud = sc.nextLine();
        }

        c = sistema.encontrarCiudadela(ciud);

        String continuar = "si";
        do {
            System.out.println("Opciones");
            System.out.println("1. Residente");
            System.out.println("2. Visitante");
            System.out.println("3. salir");
            System.out.print("Ingrese su opción:");
            n = sc.nextInt();

            switch (n) {
                case 1:
                    ingresoResidente();
                    break;
                case 2:
                    ingresoVisitante();
                    break;
                case 3:
                    System.out.println("Sesión finalizada");
                    continuar = "no";
                    break;
                default:
                    System.out.println("Opción Inválida, por favor ingrese otra opción:");
                    break;
            }
            if (n != 3) {
                System.out.println("Desea realizar otra opcion (si/no)");
                continuar = sc.nextLine();
            }

        } while (continuar.equals("si"));

    }
 /**
 * Metodo ingresoVisitante()
 * Descripcion: El sistema pedirá al visitante si tiene un codigo de acceso o no
 * este codigo fue anteriormente enviado por el sistema al correo del visitante siempre
 * y cuando el residente registro la visita anteriormente en el sistema
 * @author Grupo 7
 * 
 */
    public void ingresoVisitante() {

        char caracteres;
        String codigoA;
        int codigoAC;
        int opcion;
        do {
            System.out.println("Tiene codigo de acceso: ");
            System.out.println("1.- Si");
            System.out.println("2.- No");
            System.out.println("3.- Salir");
            System.out.println("Ingrese su opcion: ");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    conCodigo();
                    break;
                case 2:
                    sinCodigo();
                    break;
                case 3:
                    System.out.println("Sesión finalizada");
                    break;
                default:
                    System.out.println("Opción Inválida, por favor ingrese otra opción:");
                    break;
            }
        } while (opcion != 3);
    }
 /**
 * Metodo conCodigo()
 * Descripcion: El usuario ingresa el codigo de acceso, en caso que el codigo sea
 * incorrecto, el sistema volvera a pedir este codigo, caso contrario no se le permitira 
 * el ingreso a la ciudadela
 * @author Grupo 7
 * 
 */

    public void conCodigo() {
        long TInicio = System.currentTimeMillis();
        boolean va = false;
        do {
            System.out.println("Ingrese codigo de acceso");
            sc.nextLine();
            String codigo = sc.nextLine();
            Visitante v = c.encontrarVisitante(codigo);
            while(v == null){
                System.out.println("Ingrese codigo de acceso");
                sc.nextLine();
                codigo = sc.nextLine();
                v = c.encontrarVisitante(codigo);

            }
            if (v != null) {
                va = true;
                System.out.println("Acceso Concedido");
                long Tfin = System.currentTimeMillis();
                long tiempo = Tfin - TInicio;
                tiempo = tiempo / 1000;
                IngresoVisitante i = new IngresoVisitante(LocalDate.now(), "visitante", v.getNombreR(), v.getCasaVisita().getMz(), v.getCasaVisita().getVilla(), tiempo, v.getNombre());
                sistema.encontrarCiudadela(c.getNombreCiudadela()).añadirIngreso(i);
            } else {
                System.out.println("Acceso denegado");
            }

        } while (va == false);
        
    }
 /**
 * Metodo sinCodigo()
 * Descripcion: El sistema pedirá al visitante que ingrese su nombre, numero de cedula
 * nombre del residente entre otros con el que el sistema envie un correo al residente para
 * que este le envie un codigo de acceso al visitante y este pueda ingresar
 * @author Grupo 7
 * 
 */

    public void sinCodigo() {
        long TInicio = System.currentTimeMillis();
        System.out.println("Ingrese su nombre");
        sc.nextLine();
        String nombreV = sc.nextLine();
        System.out.println("Ingrese su numero de cedula");
        String cedula = sc.nextLine();
        System.out.println("Ingrese su correo");
        String correo = sc.nextLine();
        System.out.println("Ingrese nombre del residente que visita");
        String nombreR = sc.nextLine();
        System.out.println("Ingrese manzana del residente que visita");
        int mz = sc.nextInt();
        System.out.println("Ingrese villa del residente que visita");
        int villa = sc.nextInt();
        Residente r = c.encontrarResidentePorNombre(nombreR, mz, villa);
        if (r != null) {
            String codigo = Plataforma.generarClaveVisitante();
            sistema.registrarVisitante(c.getNombreCiudadela(), nombreV, cedula, correo, LocalDate.now(), codigo, r.getCasa(), nombreR);
            mail.SendMailVisita(correo, codigo);
            System.out.println("Ingrese codigo de acceso");
            String codigoAcceso = sc.nextLine();
            Visitante v = c.encontrarVisitante(codigoAcceso);
            if (v != null) {
                System.out.println("Acceso Concedido");
                long Tfin = System.currentTimeMillis();
                long tiempo = Tfin - TInicio;
                tiempo = tiempo / 1000;
                IngresoVisitante i = new IngresoVisitante(LocalDate.now(), "visitante", v.getNombreR(), v.getCasaVisita().getMz(), v.getCasaVisita().getVilla(), tiempo, v.getNombre());
                sistema.encontrarCiudadela(c.getNombreCiudadela()).añadirIngreso(i);
            } else {
                System.out.println("Acceso denegado");
            }

        }

    }

}
