/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.util.Scanner;
import inicio.Plataforma;
import inicio.Usuario;
import inicio.AdministradorCiu;
import inicio.Casa;
import inicio.Ciudadela;
import inicio.Residente;
import inicio.Visitante;
import inicio.Mail;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;

/**
 *Class IniciarSesion
 * Descripcion : Contiene como atributos objeto de tipo Scanner, Plataforma
 * Usuario and Mail, se crea su respectivo constructor, esta clase permite simular
 * la inteface de Iniciar Sesion
 * @author Grupo 7
 */

public class IniciarSesion {

    private Scanner sc;
    public Plataforma plataforma;
    private Usuario usuarioActual;
    public Mail mail;

    public IniciarSesion(Plataforma s) {
        plataforma = s;
        sc = new Scanner(System.in);
        mail = new Mail();
    }
/**
 *Metodo iniciR
 * Descripcion : Este metodo le pide al usario que ingrese un Username y su contrasena
 * y busca en la plataforma si este usuario existe, en caso que exista se rocede a pedir los 
 * mismos datos, si el usuario existe procede a buscar que tipo de usuario es, para continuar
 
 * @author Grupo 7
 */

    public void iniciar() {
        String continuar = "si";
        do {
            System.out.println("Ingrese Username: ");
            String user = sc.nextLine();
            System.out.println("Ingrese Contraseña: ");
            String contra = sc.nextLine();

            while (plataforma.encontrarUsuario(user, contra) == null) {
                System.out.println("Usuario no existe, ingrese sus credenciales de nuevo");
                System.out.println("");
                System.out.println("Ingrese Username: ");
                user = sc.nextLine();
                System.out.println("Ingrese Contraseña: ");
                contra = sc.nextLine();
            }

            usuarioActual = plataforma.encontrarUsuario(user, contra);

            System.out.println("");

            switch (usuarioActual.getNivel()) {
                case 1:

                    String continuar1 = "si";

                    do {
                        System.out.println("****Usted ha accedido como ADMINISTRADOR****");
                        System.out.println("Opciones");
                        System.out.println("1. Nuevo registro");
                        System.out.println("2. Generar reporte de uso de sistema");
                        System.out.println("3. salir");
                        System.out.println("Ingrese su opción:");
                        int opcion = sc.nextInt();

                        switch (opcion) {
                            case 1:
                                registroCiudadela();
                                break;
                            case 2:
                                reporteSistema();
                                break;
                            case 3:
                                System.out.println("Sesión finalizada");
                                continuar1 = "no";
                                break;
                            default:
                                System.out.println("Opcion incorrecta");

                        }
                        if (opcion != 3) {
                            System.out.println("Desea realizar otra opcion (si/no)");
                            sc.nextLine();
                            continuar1 = sc.nextLine();
                        }

                    } while (continuar1.equals("si"));

                    break;
                case 2:
                    String continuar2 = "si";

                    do {
                        System.out.println("****Usted ha accedido como ADMINISTRADOR DE CIUDADELA****");
                        System.out.println("CIUDADELA: " + plataforma.encontrarCiudadelaAdmin(((AdministradorCiu) usuarioActual).getNombreA()).getNombreCiudadela());
                        System.out.println("Opciones");
                        System.out.println("1. Nuevo registro");
                        System.out.println("2. Generar reporte de visitas");
                        
                        System.out.println("3. salir");
                        System.out.println("Ingrese su opción:");
                        int opcion1 = sc.nextInt();

                        switch (opcion1) {
                            case 1:
                                registroResidentes();
                                break;
                            case 2:
                                reporteDeVisita();
                                break;
                            case 3:
                                System.out.println("Sesión finalizada");
                                continuar2 = "no";
                                break;
                            default:
                                System.out.println("Opcion incorrecta");

                        }

                        if (opcion1 != 3) {
                            System.out.println("Desea realizar otra opcion (si/no)");
                            sc.nextLine();
                            continuar2 = sc.nextLine();
                        }

                    } while (continuar2.equals("si"));

                    break;
                case 3:

                    String continuar3 = "si";

                    do {
                        System.out.println("****Usted ha accedido como RESIDENTE****");
                        System.out.println("Opciones");
                        System.out.println("1. Nuevo registro de visitante");
                        System.out.println("2. Ver listado de visitantes activos");
                        System.out.println("3. Eliminar refistro de visitante");
                        System.out.println("4. Cambiar pin");
                        System.out.println("5. salir");
                        System.out.println("Ingrese su opción:");
                        int opcion2 = sc.nextInt();

                        switch (opcion2) {
                            case 1:
                                registroVisitantes();
                                break;
                            case 2:
                                verListaVisitante();
                                break;
                            case 3:
                                eliminarRegistro();
                                break;
                            case 4:
                                cambiarPin();
                                break;
                            case 5:
                                System.out.println("Sesión finalizada");
                                continuar3 = "no";
                                break;
                            default:

                                System.out.println("Opcion incorrecta");

                        }

                        if (opcion2 != 5) {
                            System.out.println("Desea realizar otra opcion (si/no)");
                            sc.nextLine();
                            continuar3 = sc.nextLine();
                        }

                    } while (continuar3.equals("si"));

                    break;
                case 4:

                    break;
                default:
                    System.out.println("Opcion incorrecta");

            }

            if (continuar != "no") {
                System.out.println("Desea iniciar sesion con otra cuenta(si/no): ");
                sc.nextLine();

                continuar = sc.nextLine();
            }

        } while (continuar.equals("si"));

    }

    //METODOS DE LA INTERFAZ
    /**
 *Metodo reporteSistema()
 * Descripcion : genera el reporte del sistema
 * @author Grupo 7
 */

    
    public void reporteSistema() {
        plataforma.generarReporteSistema();
    }

    /**
 *Metodo reporteDeVisita()
 * Descripcion : genera el reporte de visitas del sistema, le pide al usuario
 * que ingrese que tipo de opcion desea realizar y simplemente imprime por pantalla
 * la opcion seleccionada
 * @author Grupo 7
 */
    
    public void reporteDeVisita() {

        
        Ciudadela c = plataforma.encontrarCiudadelaAdmin(((AdministradorCiu) usuarioActual).getNombreA());
        
        System.out.println("Filtrar por:");
        System.out.println("1. Usar todas los ingresos");
        System.out.println("2. Rango de fechas");
        System.out.println("3. Tipo");
        System.out.println("4. Regresar");

        int opcion = sc.nextInt();

        switch (opcion) {
            case 1:
                
                c.reporteCompleto();
                break;
            case 2:
                System.out.println("Metodo no disponible");;
                break;
            case 3:
                System.out.println("Ingrese tipo:");
                sc.nextLine();
                String tipo = sc.nextLine();
                c.reporteFiltroTipo(tipo);
                break;
            case 4:
                System.out.println("Regresando al menu anterior");
                break;
            default:
                System.out.println("Opcion incorrecta");
                break;

        }

    }
/**
 *Metodo registroCiudadela()
 * Descripcion : Este metodo le pertenece al usuario tipo administrador
 * el cual se encarga de registrar la ciudadela en el sistema, el sistema le pide 
 * a este que ingrese los datos y luego crea el objeto de tipo ciudadela y la agg 
 * al sistema, ademas envia un correo al administrador de esta ciudadela, informando
 * cual es su usuario y contrasena
 * @author Grupo 7
 */   
    public void registroCiudadela() {

        System.out.println("");
        System.out.println("Usted ha accedido como ADMINISTRADOR");
        System.out.println("");
        System.out.println("*REGISTRO DE CIUDADELA*");
        System.out.println("");
        System.out.println("DATOS DE LA CIUDADELA");
        System.out.println("Ingrese nombre de nueva ciudadela");
        sc.nextLine();
        String nombreCiu = sc.nextLine();
        System.out.println("Ingrese razon social");
        String razon = sc.nextLine();
        System.out.println("Ingrese numero de Ruc");
        String ruc = sc.nextLine();
        System.out.println("Ingrese ubicacion");
        String ubicacion = sc.nextLine();
        System.out.println("");
        System.out.println("DATOS DEL ADMINSITRADOR");
        System.out.println("");
        System.out.println("Ingrese nombre del administrador");
        String nombreAdmin = sc.nextLine();
        System.out.println("Ingrese identificacion");
        String identidad = sc.nextLine();
        System.out.println("Ingrese correo");
        String correo = sc.nextLine();
        AdministradorCiu admin = new AdministradorCiu(nombreAdmin, identidad, correo, "admin" + nombreCiu, "admin" + nombreCiu);
        mail.SendMailUsuario(correo, "admin" + nombreCiu, "admin" + nombreCiu);
        plataforma.registrarCiudadela(nombreCiu, razon, ruc, ubicacion, admin);

    }
/**
 *Metodo registroResidentes()
 * Descripcion : similar al metodo registrarCiudadela, con la diferencia
 * que esta ves el sistema le pide al administrador de ciudadela que ingrese 
 * datos o atributos del residente que va a registrar. Ademas el sistema
 * envia al correo del residente un usuario y una contrasena
 * @author Grupo 7
 */   
    public void registroResidentes() {

        System.out.println("");
        System.out.println("*REGISTRO DE RESIDENTE*");
        System.out.println("");
        System.out.println("DATOS DEL RESIDENTE");
        System.out.println("Ingrese nombre del residente");
        sc.nextLine();
        String nombreR = sc.nextLine();
        System.out.println("Ingrese cedula");
        String ced = sc.nextLine();
        System.out.println("Ingrese correo");
        String correo = sc.nextLine();
        System.out.println("Ingrese telefono");
        String tel = sc.nextLine();
        System.out.println("");
        System.out.println("DATOS DE LA CASA");
        System.out.println("Mz:");
        int mz = sc.nextInt();
        System.out.print("");
        System.out.println("Villa");
        int villa = sc.nextInt();
        String[] parts = nombreR.split(" ");
        String user = parts[0];
        String contra = Plataforma.generarClaveUsuario();
        String ciudadela = plataforma.encontrarCiudadelaAdmin(((AdministradorCiu) usuarioActual).getNombreA()).getNombreCiudadela();
        plataforma.registrarResidente(ciudadela, nombreR, correo, ced, tel, new Casa(mz, villa), "0000", user, contra);
        mail.SendMailUsuario(correo, user, contra);

    }

/**
 *Metodo registroVisitantes()
 * Descripcion : similar al metodo registrarCiudadela, con la diferencia que este metodo
 * le pertenece al usuario de tipo Residente, el cual va a registrar una proxima visita
 * @author Grupo 7
 */   
    public void registroVisitantes() {
        String nombreVi, ciV, correoVi, codigo, fechai;
        System.out.println("Ingrese nombre del visitante: ");
        sc.nextLine();
        nombreVi = sc.nextLine();
        System.out.println("Ingrese ci: ");
        ciV = sc.nextLine();
        System.out.println("Ingrese su correo");
        correoVi = sc.nextLine();
        System.out.println("Ingrese fecha de ingreso(dd/MM/yyyy)");
        fechai = sc.nextLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate ingreso = LocalDate.parse(fechai, formatter);
        String codigoingreso = Plataforma.generarClaveVisitante();
        String ciudadela = plataforma.encontrarCiudadelaResidente(((Residente) usuarioActual).getNombreResidente()).getNombreCiudadela();
        String residenteVisitar = ((Residente) usuarioActual).getNombreResidente();
        Casa casaAVisitar = ((Residente) usuarioActual).getCasa();
        plataforma.registrarVisitante(ciudadela, nombreVi, ciV, correoVi, ingreso, codigoingreso, casaAVisitar, residenteVisitar);
        mail.SendMailVisita(correoVi, codigoingreso);
    }

/**
 *Metodo verListaVisitante()
 * Descripcion : Metodo que le pertenece al usuario de tipo residente, el cual podra ver 
 * su lista de visitas
 * @author Grupo 7
 */   
    public Ciudadela verListaVisitante() {
        plataforma.depurarListaVisitante();
        Ciudadela ciu = plataforma.encontrarCiudadelaResidente(((Residente) usuarioActual).getNombreResidente());
        ciu.mostrarVisitante();
        return ciu;
    }
/**
 *Metodo eliminarRegistro()
 * Descripcion : Metodo que le pertenece al usuario de tipo residente, el cual podra eliminar 
 * una visita del sistema
 * @author Grupo 7
 */   
    public void eliminarRegistro() {
        Ciudadela ciu = verListaVisitante();
        System.out.println("Ingrese el numero de visitante que desea borrar:");
        int indice = sc.nextInt();
        ciu.getVisitantes().remove(indice - 1);
    }
/**
 *Metodo cambiarPin()
 * Descripcion : Metodo que le pertenece al usuario de tipo residente, el cual
 * podra cambiar su pin y registrar uno nuevo
 * @author Grupo 7
 */   
    public void cambiarPin() {
        Residente r = (Residente) usuarioActual;
        System.out.println("Pin actual" + r.getPin());
        System.out.println("Ingrese nuevo pin");
        String nuevo = sc.nextLine();
        r.setPin(nuevo);
    }

    
}
