package inicio;
import java.time.LocalDate;
/**
 * Class Ingreso
 * Descripcion: Clase padre, se heredan de este las clases IngresoVehiculo 
 * e ingresoVisitante, contiene sus respectivos Constructores y metodos basicos(getters y setter)
 * @version 
 * @since 
 * @author Grupo 7
 * 
 */

public class Ingreso {    
    private long segundos;
    private LocalDate fecha;
    private String tipo; 
    private String nombreResidente;
    private int mz;
    private int villa;
    
    public Ingreso(LocalDate fecha,String tipo, String nombreR, int mz,int villa, long segundos){
        this.fecha = fecha;
        this.tipo = tipo;
        nombreResidente = nombreR;
        this.mz = mz;
        this.villa = villa;
        this.segundos = segundos;
    }
    
    public Ingreso(){}

    public long getSegundos() {
        return segundos;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public String getNombreResidente() {
        return nombreResidente;
    }

    public int getMz() {
        return mz;
    }

    public int getVilla() {
        return villa;
    }
    
    public String[] getLinea(){
        String[] linea = {fecha.toString(),tipo ,nombreResidente, Integer.toString(mz),Integer.toString(villa),"NO USO VEHICULO"};
        return linea;

}
    
   
    
}
