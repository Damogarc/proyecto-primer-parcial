/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;


import java.time.LocalDate;


/**
 * Class de Administrador de ciudadela
 * Contiene: sus respectivos Constructores y metodos basicos(getters y setter)
 * @version 
 * @since 
 * @author Grupo 7
 * 
 */
public class AdministradorCiu extends Usuario{
    private String correoAC;
    private String nombreA;
    private String identificacionA;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    
    
    
    public AdministradorCiu(String correoAC, String noombreA, String identificacionA, String user, String contra) {
        super(user, contra, 2);
        this.correoAC = correoAC;
        this.nombreA = noombreA;
        this.identificacionA = identificacionA;
        
        
    }         
    /**
     * Metodo getters para obtener el correo del administrador
     * @return correo del administrador 
     */
    public String getCorreoAC() {
        return correoAC;
    }

    public void setCorreoAC(String correoAC) {
        this.correoAC = correoAC;
    }

    public String getNombreA() {
        return nombreA;
    }

    public void setNombreA(String nombreA) {
        this.nombreA = nombreA;
    }

    public String getIdentificacionA() {
        return identificacionA;
    }

    public void setIdentificacionA(String identificacionA) {
        this.identificacionA = identificacionA;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }
    
    
    

}