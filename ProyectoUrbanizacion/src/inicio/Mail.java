/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *Class Mail
 * Descripcion: Esta clase ayuda a enviar todos los correos que se utilizan en este
 * proyecto
 * @author Luis_
 */
public class Mail {

    private Properties properties = new Properties();

    private String password;

    private Session session;

 public void SendMailUsuario(String receptor,String usuario,String contra) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
 
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("sistemaingresog7@gmail.com", "sistemag7");
                    }
                });
 
        try {
 
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sistemaingresog7@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(receptor));
            message.setSubject("Usuario y contraseña");
            message.setText("Bienvenido al sistema, sus datos son:  USUARIO: " + usuario +" CONTRASEÑA: " + contra);
 
            Transport.send(message);
            System.out.println("Mensaje eviado con exito");
 
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
 
 public void SendMailVisita(String receptor,String codigo) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
 
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("sistemaingresog7@gmail.com", "sistemag7");
                    }
                });
 
        try {
 
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sistemaingresog7@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(receptor));
            message.setSubject("Codigo de acceso");
            message.setText("Su codigo de acceso es :" + codigo);
 
            Transport.send(message);
            System.out.println("Visitante registrado con exito, codigo enviado");
 
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}