/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import com.opencsv.CSVWriter;
import java.util.ArrayList;
import inicio.Residente;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Class Plataforma
 * Descripcion: tambien conocida como papa, mami lo que sea, sin esta no hacemos nada
 * @author Grupo 7
 */
public class Plataforma {

    //Debe iniciar con 5 casas, un recidente, un administrador
    public ArrayList<Ciudadela> ciudadelas;
    public ArrayList<Usuario> usuarios;
    public ArrayList<Visitante> visitantes;

    public Plataforma() {
        ciudadelas = new ArrayList<>();
        usuarios = new ArrayList<>();
        inicializarSistema();
        usuarios.add(new AdministradorDeSistema());

        visitantes = new ArrayList<>();

    }
    /**
 *Metodo encontrarUsuario
 * Descripcion: Busca en el sistema o plataforma un usuario, 
 * y recibe como parametro String user, String contra
 * @author Grupo 7
 * @return un objeto de tipo Usuario
 */

    public Usuario encontrarUsuario(String user, String contra) {
        for (Usuario u : usuarios) {
            if (u.getUsername().equals(user) && u.getPassword().equals(contra)) {
                return u;
            }
        }
        return null;
    }
/**
 *Metodo enconCiudadela
 * Descripcion: Busca en el sistema o plataforma un objeto de tipo Ciudadela, 
 * y recibe como parametro String bombre
 * @author Grupo 7
 * @return un objeto de tipo Ciudadela
 */
    public Ciudadela encontrarCiudadela(String nombre) {
        for (Ciudadela c : ciudadelas) {
            if (c.getNombreCiudadela().equals(nombre)) {
                return c;
            }
        }
        return null;
    }
/**
 *Metodo encontrarCiudadelaAdmin
 * Descripcion: Busca en el sistema o plataforma una Ciudadela, 
 * y recibe como parametro String nombre de la ciudadela
 * @author Grupo 7
 * @return un objeto de tipo Ciudadela
 */
    public Ciudadela encontrarCiudadelaAdmin(String nombre) {
        for (Ciudadela c : ciudadelas) {
            if (c.getAdmin().getNombreA().equals(nombre)) {
                return c;
            }
        }
        return null;
    }
/**
 *Metodo encontrarCiudadelaResidente
 * Descripcion: Busca en el sistema o plataforma un objeto de tipo Ciudadela, 
 * y recibe como parametro String nombre
 * @author Grupo 7
 * @return un objeto de tipo Ciudadela
 */
    public Ciudadela encontrarCiudadelaResidente(String nombre) {
        for (Ciudadela c : ciudadelas) {
            for (Residente r : c.getResidentes()) {
                if (r.getNombreResidente().equals(nombre)) {
                    return c;
                }
            }

        }
        return null;
    }
/**
 *Metodo registrarCiudadela
 * Descripcion: Recibe atributos conocidos del objeto ciudadela
 * Verifica si no se encuentra registrada la ciudadela en el sistema
 * en caso que no este agrega esta ciudadela se la agg a la lista de ciudadelas, 
 * y a la ves se agrega al admin a la lista de usuarios
 * @author Grupo 7
 * @return un objeto de tipo Ciudadela
 */
    

    public boolean registrarCiudadela(String nombre, String RazonSocial, String ruc, String ubicacion, AdministradorCiu admin) {
        Ciudadela Registro = encontrarCiudadela(nombre);
        if (Registro == null) {
            Ciudadela nueva = new Ciudadela(nombre, RazonSocial, ruc, ubicacion, admin);
            ciudadelas.add(nueva);
            usuarios.add(admin);
            return true;
        }
        return false;
    }
/**
 *Metodo registrarResidente
 * Descripcion: Recibe atributos conocidos del objeto ciudadela y resideente,
 * crea un objeto de tipo ciudadela, y este objeto llama al metodo de registrar residente
 * en la ciudadela y si esto se cumple se agg el residente en la lista de usuarios
 * y a la ves se agrega al admin a la lista de usuarios. Retorna un boolean
 * @author Grupo 7
 * @return un boolean
 */
    public boolean registrarResidente(String nombre, String nombreResidente, String correoResidente, String cedula, String telefono, Casa casa, String pin, String user, String password) {
        Ciudadela ciu = encontrarCiudadela(nombre);
        if (ciu.registrarResidente(nombreResidente, correoResidente, cedula, telefono, casa, pin, user, password)) {
            usuarios.add(ciu.encontrarResidente(pin,cedula));
            return true;
        }
        return false;
    }
/**
 *Metodo registrarVisitante
 * Descripcion: Similar al metodo registrarResidente, la diferencia es que en este caso
 * se agg un objeto de tipo visitante a la lista de visitantes.
 * @author Grupo 7
 * @return un boolean
 */
    
    public boolean registrarVisitante(String nombreCiu, String nombre, String cedula, String correo, LocalDate fecha, String codigo, Casa casa, String nombreR) {
        Ciudadela ciu = encontrarCiudadela(nombreCiu);
        if(ciu.registrarVisitante(nombre,cedula,correo,fecha,codigo, casa, nombreR)){
            visitantes.add(ciu.encontrarVisitante(cedula));
            return true;
        }
        return true;
    }
    
    public void registrarIngreso(Ingreso i, Ciudadela c) {
        c.añadirIngreso(i);
    }
    /**
 *Metodo depurarListaVisitante()
 * Descripcion: Ayuda a la eliminacion de los visitantes registrados por los residentes
 * esta se eliminaran automaticamente del sistema cada cierto tiempo.
 * @author Grupo 7
 * @return no retorna nada
 */

    
    
    public void depurarListaVisitante(){
    for(Ciudadela c: ciudadelas){
        for(Visitante v: c.getVisitantes()){
            LocalDateTime comprobar = v.getFecha();
            LocalDateTime actual = LocalDateTime.now();
            Duration duration = Duration.between(actual,comprobar);
            long horas = duration.toHours();
            if(horas >= 12){
                c.getVisitantes().remove(v);
                visitantes.remove(v);
            }
        }
    }
    
    
    }
    /**
 *Metodo generarReporteSistema()
 * Descripcion: Ayuda a generar el reporte para el csv
 * @author Grupo 7
 * @return un boolean
 */

    public void generarReporteSistema(){
        
            final String[] encabezado = {"Nombre de la ciudadela", "razón social", "número ingreso visitantes", "tiempo promedio ingreso visitantes", "número de ingresos residente", "tiempo promedio ingreso residentes"};
            final Character delimiter = ';';//Delimitador, por defecto es ","
            String archivoCSV = "C:\\Users\\Luis_\\OneDrive\\Documents\\ReporteDeSistema.csv";//Nombre de archivo CSV
            ArrayList<String[]> lineas = new ArrayList<String[]>();
            lineas.add(encabezado);
            for(Ciudadela c: ciudadelas){
                String [] linea = {c.getNombreCiudadela(), c.getRazonSocial() , Integer.toString(c.numeroVisitas()) , String.valueOf(c.tiempoIngresoVisitas()), Integer.toString(c.numeroIngresosResidente()),String.valueOf(c.tiempoIngresoResidente())};
                lineas.add(linea);
            }
        try {    
            CSVWriter writer = new CSVWriter(new FileWriter(archivoCSV));
            
            writer.writeAll(lineas);
            
            writer.close();
            System.out.println("archivo generado");
        } catch (IOException ex) {
            System.out.println("Fallo al generar archivo");
        }
        
        
    }
    
    
    
    
    /**
 *Metodo generarClaveVisitante 
 * Descripcion: Metodo static que podra ser accedido por todas las clases, genera
 * un codigo aleatorio para los visitantes
 * @author Grupo 7
 * @return un String 
 */


    public static String generarClaveVisitante() {
        String clave = UUID.randomUUID().toString().toUpperCase().substring(0, 8);
        System.out.println(clave);
        return clave;

    }
    /**
 *Metodo generarClaveUsuario 
 * Descripcion: Metodo static que podra ser accedido por todas las clases, genera
 * un codigo aleatorio para los usuarios
 * @author Grupo 7
 * @return un String 
 */
    public static String generarClaveUsuario() {
        String clave = UUID.randomUUID().toString().toUpperCase().substring(0, 4);
        System.out.println(clave);
        return clave;

    }
    /**
 *Metodo inicializarSistema() 
 * Descripcion: Metodo por defecto que genera los residentes, casas, ciudadelas, administradores 
 * por defecto en la plataforma
 * @author Grupo 7
 *  
 */
    private void inicializarSistema() {
        
        AdministradorCiu admin1 = new AdministradorCiu("Roberto Quijano", "1234", "rquijano@hotmail.com", "adminCataluña", "adminCataluña");
        AdministradorCiu admin2 = new AdministradorCiu("Ariana Cepdeda", "1235", "acepeda@hotmail.com", "adminMilann", "adminMilann");
        AdministradorCiu admin3 = new AdministradorCiu("Ana Posligua", "1239", "aposligua@hotmail.com", "adminMetropolis1", "adminMetropolis1");
        AdministradorCiu admin4 = new AdministradorCiu("Andres Valbuena", "1232", "avalbuena@hotmail.com", "adminMetropolis2", "adminMetropolis2");
        registrarCiudadela("Cataluna", "Privada", "222", "Orquideas", admin1);
        registrarCiudadela("Milann", "Publica", "111", "Orquideas", admin2);
        registrarCiudadela("Metropolis2", "Privada", "333", "Autopista", admin4);
        registrarCiudadela("Metropolis1", "Publica", "444", "Autopista", admin3);
        registrarResidente("Metropolis1", "Daniela Garcia", "correo1", "0000", "09999", new Casa(1, 1), "1234", "Dani", "Dani");
        encontrarCiudadela("Metropolis1").encontrarResidente("1234","0000").RegistrarVehiculo(new Vehiculo("1234", "Daniela"));
        registrarResidente("Cataluna", "Kevin Lopez", "correo2", "12245", "09998", new Casa(1, 2), "1224", "Kevin", "Kevin");
        registrarResidente("Cataluna", "Daniel Moran", "correo3", "12225", "09997", new Casa(1, 3), "1222", "Daniel", "Daniel");
        registrarResidente("Cataluna", "Martina Saavedra", "correo4", "12222", "09996", new Casa(1, 4), "1334", "Marti", "Marti");
        registrarResidente("Milann", "Diego Jacome", "correo5", "22222", "09995", new Casa(1, 5), "1333", "Diego", "Diego");
        registrarResidente("Metropolis2", "Gaby Rodriguez", "correo6", "13345", "09994", new Casa(1, 6), "1244", "Gab", "Gab");

    }

        /**
 *Metodo consultarCodigoVisitante 
 * Descripcion: Verifica si el codigo pasado como parametro existe y es igual 
 * al atributo del objeto visita
 * @author Grupo 7
 * @return un String 
 */
    

    public boolean consultarCodigoVisitante(String codigoC) {
        for (Visitante visita : visitantes) {
            if (visita.getCodigo().equals(codigoC)) {
                return true;
            }
        }
        return false;
    }

    

}