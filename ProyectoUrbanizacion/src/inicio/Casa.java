/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;
/**
 * Class Casa
 * Contiene: sus respectivos Constructores y metodos basicos(getters y setter)
 * @version 
 * @since 
 * @author Grupo 7
 * 
 */

public class Casa {
    private int Mz, villa;

    public Casa(int Mz, int villa) {
        this.Mz = Mz;
        this.villa = villa;
    }

    public Casa() {
    }
    
    

    

    public int getVilla() {
        return villa;
    }
    
    @Override
    public String toString(){
        return "Mz: " + Mz + " Villa " + villa;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj!= null){
            if(obj instanceof Casa){
                Casa c = (Casa)obj;
                if(this.Mz == c.getMz()){
                    return true;
                }
            }
        }
        return false;
    }

    public int getMz() {
        return Mz;
    }

    
    
}

