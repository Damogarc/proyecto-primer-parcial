/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inicio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


/**
 *Class Usuario 
 * Descripcion : Contiene sus respectivos constructores, metodos getters and setters 
 * @author gRUPO 7
 */
public class Visitante{
    
    private String nombre;
    private String cedula;
    private String correo;
    private LocalDateTime fecha;
    private String codigo;
    private Casa casaVisita;
    private String nombreR;

    public Visitante(String nombre, String cedula, String correo, LocalDate fecha, String codigo, Casa casa, String nombreR) {
        LocalTime local = LocalTime.of(0,0,0,0);
        this.fecha = LocalDateTime.of(fecha, local);
        this.nombre = nombre;
        this.cedula = cedula;
        this.correo = correo;
        this.codigo = codigo;
        casaVisita = casa;
        this.nombreR = nombreR;
    }

    public Visitante() {
    }

    public Casa getCasaVisita() {
        return casaVisita;
    }

    public String getNombreR() {
        return nombreR;
    }
   
    
    public String getNombre() {
        return nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    
    
    
    
    
    
    
}