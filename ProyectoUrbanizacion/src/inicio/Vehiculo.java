/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

/**
 *Class Vehiculo
 * Descripcion : Contiene sus respectivos constructores, metodo getters and setters 
 * @author gRUPO 7
 */
public class Vehiculo {
 
    private String matricula;
    private String propietario;
    
    public Vehiculo(String mat, String pro){
        matricula = mat;
        propietario = pro;
    }

    public Vehiculo() {
    }
    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getPropietario() {
        return propietario;
    }

    
            
    
}
