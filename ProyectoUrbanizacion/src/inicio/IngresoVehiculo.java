/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import java.time.LocalDate;

/**
 * Class Ingreso
 * Descripcion: Es clase hija de la clase Ingreso, 
 * contiene sus respectivos Constructores y metodos basicos(getters y setter)
 * @version 
 * @since 
 * @author Grupo 7
 * 
 */

public class IngresoVehiculo extends Ingreso{
    
    private String matricula;
    
    public IngresoVehiculo(LocalDate fecha,String tipo, String nombreR, int mz,int villa, long segundos, String matricula){
        super(fecha,tipo, nombreR, mz,villa, segundos);
        this.matricula = matricula;
    }
    
    public String getMatricula() {
        return matricula;
    }
    
    public String[] getLinea(){
        String[] linea = {super.getFecha().toString(),super.getTipo() ,super.getNombreResidente(), Integer.toString(super.getMz()),Integer.toString(super.getVilla()),matricula};
        return linea;
}
    
}
