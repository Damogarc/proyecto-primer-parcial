/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import java.util.ArrayList;
import java.util.Scanner;
/**
 *Class Residente
 * Es una de las clases principales de este proyecto, contiene sus respectivos constructores
 * metodos getters, setter toString
 * @author Grupo7
 */
public class Residente extends Usuario{
    private String nombreResidente, correoResidente,cedula, telefono, pin;
    Casa casa;
    ArrayList<Vehiculo> vehiculos;

    public Residente(String nombreResidente, String correoResidente,String cedula, String telefono, Casa casa, String pin, String user,String password) {
        super(user, password,3);
        this.nombreResidente = nombreResidente;
        this.correoResidente = correoResidente;
        this.cedula = cedula;
        this.telefono = telefono;
        this.casa = casa;
        this.pin = pin;
        vehiculos = new ArrayList<>();
    }

    public Residente(String nombreResidente, String correoResidente,String cedula, String telefono, Casa casa, String pin) {
        super(3);
        this.nombreResidente = nombreResidente;
        this.correoResidente = correoResidente;
        this.cedula = cedula;
        this.telefono = telefono;
        this.casa = casa;
        this.pin = pin;
        vehiculos = new ArrayList<>();
    }
    
    public Residente(String nombreResidente) {
        super(3);
        this.nombreResidente = nombreResidente;
    }
    
    
    
    
 
    public boolean RegistrarVisitante(Visitante v){
        return false;
    };
    
    /**
     * Metodo registrarVehiculo
     * Descripcion: Recibe como parametro un objeto de tipo Vehiculo
     * si el objeto no es de tipo null se lo agg al arreglo de vehiculos, 
     * que contiene el residente
     * @return true o false
     */
    public boolean RegistrarVehiculo(Vehiculo v){
        if(v!=null){
            vehiculos.add(v);
            return true;
            
        } else{
        return false;}
    }
    /*verVisitante() este metodo ayudara ver la informacion de los visitantes registrados
    en la plataforma
    */
    /*eliminarRegistroVisitante() ayudara a la eliminacion del visitante siempre 
    y cuando este haya sido registrado
    */
    
    
    public void setDatos(String user,String clave){
        super.setUsername(user);
        super.setPassword(clave);
    }
    
    public String getNombreResidente() {
        return nombreResidente;
    }

    public String getCorreoResidente() {
        return correoResidente;
    }

    public String getCedula() {
        return cedula;
    }
    

    public String getTelefono() {
        return telefono;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Casa getCasa() {
        return casa;
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }
    
    /**
     * Metodo cambiarPin
     * Descripcion: el residente tendra el metodo para cambiar el pin cuando el 
     * lo desee
     * No retorna nada
     */
    
    
    public void CambiarPin(){
        System.out.println("Ingrese su nuevo pin");
        Scanner sc = new Scanner(System.in);
        String nuevaclave = sc.nextLine();
        pin = nuevaclave;
    }
   
    @Override
    public String toString(){
        return "Nombre: " + nombreResidente ;
    
    }
    
}
