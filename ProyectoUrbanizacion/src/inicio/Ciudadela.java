/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import org.joda.time.DateTime;
import static org.joda.time.DateTimeFieldType.year;
import static org.joda.time.format.ISODateTimeFormat.year;


/**
 * Class de Ciudadela
 * Descripcion: sus respectivos Constructores y metodos basicos(getters y setter)
 * @version 
 * @since 
 * @author Grupo
 * 
 */

public class Ciudadela {

    private String nombreCiudadela;
    private String razonSocial;
    private String numRuc;
    private String ubicacion;
    private ArrayList<Visitante> visitantes;
    private ArrayList<Residente> residentes;
    private ArrayList<Ingreso> ingresos;
    private AdministradorCiu admin;

    public Ciudadela(String nombreCiudadela, String razonSocial, String numRuc, String ubicacion, AdministradorCiu admin) {
        this.nombreCiudadela = nombreCiudadela;
        this.razonSocial = razonSocial;
        this.numRuc = numRuc;
        this.ubicacion = ubicacion;
        this.admin = admin;
        this.visitantes = new ArrayList<>();
        this.residentes = new ArrayList<>();
        this.ingresos = new ArrayList<>();
    }
    
    
    public Ciudadela(){
        this.visitantes = new ArrayList<>();
        this.residentes = new ArrayList<>();
        this.ingresos = new ArrayList<>();
    
    }

    public Ciudadela(String nombreCiudadela) {
        this.nombreCiudadela = nombreCiudadela;
    }

    public AdministradorCiu getAdmin() {
        return admin;
    }
    /**
 * Metodo encontrar visitante
 * Descripcion: Ayuda a encontrar un visitante pasandole como parametro String codigo
 * @version 
 * @since 
 * @return retorna un objeto de tipo Visitante
 * @author Grupo
 * 
 */

    public Visitante encontrarVisitante(String codigo) {
        for (Visitante v : visitantes) {
            if (v.getCodigo().equals(codigo)) {
                return v;
            }
        }
        return null;
    }
/**
 * Metodo registrar visitante
 * Descripcion: Ayuda a registrar un visitante pasandole como parametro los atributos 
 * que contiene un objeto de tipo visitantte
 * @version 
 * @since 
 * @return retorna un boolean
 * 
 */
    public boolean registrarVisitante(String nombre, String cedula, String correo, LocalDate fecha, String codigo, Casa casa, String nombreR) {
        Visitante Registro = encontrarVisitante(cedula);
        if (Registro == null) {
            Visitante nuevo = new Visitante(nombre, cedula, correo, fecha, codigo, casa, nombreR);
            visitantes.add(nuevo);
            return true;
        }
        return false;
    }
/**
 * Metodo encontrarResidente(String pin, String cedula)
 * Descripcion: ayuda a encontrar un residente en el arreglo residente, este siendo 
 * atributo de la clase ciudadela, resibe como parametros un Strin pin y un
 * String cedula
 * @version 
 * @since 
 * @return retorna un objeto de tipo residente
 * @author Grupo
 * 
 */
    public Residente encontrarResidente(String pin, String cedula) {
        for (Residente r : residentes) {
            if (r.getCedula().equals(cedula) && r.getPin().equals(pin)) {
                return r;
            }
        }
        return null;
    }
/**
 * Metodo encontrarResidentePorVehiculo(String num)
 * Descripcion: similar al metodo encontrarResidente, pero con la diferencia que resibe como
 * parametro un String num
 * @version 
 * @since 
 * @return retorna un objeto de tipo residente
 * @author Grupo
 * 
 */
    public Residente encontrarResidentePorVehiculo(String num) {
        for (Residente r : residentes) {
            for (Vehiculo v : r.vehiculos) {
                if (v.getMatricula().equals(num)) {
                    return r;
                }

            }
        }
        return null;
    }
/**
 * Metodo encontrarResidentePorNombre(String num)
 * Descripcion: similar al metodo encontrarResidente, pero con la diferencia que resibe como
 * parametro un String num, int mz, int villa
 * @version 
 * @since 
 * @return retorna un objeto de tipo residente
 * @author Grupo
 * 
 */    
    public Residente encontrarResidentePorNombre(String nombre, int mz, int villa) {
        Casa c = new Casa(mz,villa);
        for (Residente r : residentes) {
            if(r.getCasa().equals(c) && r.getNombreResidente().equals(nombre)){
            return r;
            }
        }
        return null;
    }
/**
 * Metodo registrarResidente
 * Descripcion: Verifica en el registro si ese residente no existe, en caso que no 
 * exista se crea un objeto de tipo Residente y es agregado al arreglo de residentes
 * @version 
 * @since 
 * @return retorna un boolean
 * @author Grupo
 * 
 */

    
    public boolean registrarResidente(String nombreResidente, String correoResidente, String cedula, String telefono, Casa casa, String pin, String user, String password) {
        Residente Registro = encontrarResidente(pin, cedula);
        if (Registro == null) {
            Residente nuevo = new Residente(nombreResidente, correoResidente, cedula, telefono, casa, pin, user, password);
            residentes.add(nuevo);
            return true;
        }
        return false;
    }

    public void añadirIngreso(Ingreso i) {
        ingresos.add(i);
        System.out.println("ingreso nuevo exitoso");
    }

    public String getNombreCiudadela() {
        return nombreCiudadela;
    }

    public void setNombreCiudadela(String nombreCiudadela) {
        this.nombreCiudadela = nombreCiudadela;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNumRuc() {
        return numRuc;
    }

    public void setNumRuc(String numRuc) {
        this.numRuc = numRuc;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public ArrayList<Residente> getResidentes() {
        return residentes;
    }

    public ArrayList<Visitante> getVisitantes() {
        return visitantes;
    }

    public ArrayList<Ingreso> getIngresos() {
        return ingresos;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj instanceof Ciudadela) {
                Ciudadela c = (Ciudadela) obj;
                if (c.getNombreCiudadela().equals(nombreCiudadela)) {
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public String toString() {
        return "Ciudadela: " + nombreCiudadela + ", Razon Social: " + razonSocial + ", RUC: " + numRuc + ", Ubicacion: " + ubicacion;

    }
/**
 * Metodo mostrarVisitante()
 * Descripcion: No resibe parametros, recorre la lista de visitantes 
 * e imprime el nombre de cada uno de los visittantes, no retorna nada.
 * @version 
 * @since 
 * @return retorna un boolean
 * @author Grupo 7
 * 
 */

    public void mostrarVisitante() {

        System.out.println("Los visitantes registrados en la ciudadela " + getNombreCiudadela() + " son:");

        for (Visitante v : visitantes) {
            int contador = 1;
            System.out.println(contador + ") " + v.getNombre() + ",   ");
        }
        System.out.println("");
    }
/**
 * Metodo numeroVisitas()
 * Descripcion: No resibe parametros, recorre la lista de ingresos 
 * y las suma por cada iteracion, retorna un total.
 * @version 
 * @since 
 * @return retorna un int
 * @author Grupo 7
 * 
 */
    public int numeroVisitas() {
        int total = 0;
        
        for (Ingreso i : ingresos) {
            if (i.getTipo().equals("visitante")) {

                total += 1;
            }
        }
        
        return total;
        
    }
/**
 * Metodo numeroIngresosResidente()
 * Descripcion: No recibe parametros, recorre la lista de ingresos 
 * y las suma por cada iteracion, retorna un total de residente.
 * @version 
 * @since 
 * @return retorna un int
 * @author Grupo 7
 * 
 */    
    public int numeroIngresosResidente() {
        int total = 0;

        for (Ingreso i : ingresos) {
            if (i.getTipo().equals("residente")) {

                total += 1;
            }
        }
        return total;
    }
/**
 * Metodo tiempoIngresoVisitas()
 * Descripcion: No recibe parametros, recorre la lista de ingresos 
 * obtiene el tipo y compara con visitante, si esto es verdad, realiza la suma y 
 * las almacena en un total, y va sumando cada iteracion, en caso que no existan 
 * datos y el promedio es cero, se captura esta exceptions
 * @return retorna un double promedio
 * @author Grupo 7
 * 
 */
    public double tiempoIngresoVisitas() {
        double promedio;
        int contador = 0;
        long total = 0;
        for (Ingreso i : ingresos) {
            if (i.getTipo().equals("visitante")) {
                long tiempo = i.getSegundos();
                total += tiempo;
                contador += 1;
            }
        }
        try{
        
        promedio = (total / contador);
                }
        catch(Exception e){
        promedio = 0;
        }
        return promedio;
    }
/**
 * Metodo tiempoIngresoResidente()
 * Descripcion: No recibe parametros, recorre la lista de ingresos 
 * obtiene el tipo y compara con residente, si esto es verdad, realiza la suma y 
 * las almacena en un total, y va sumando cada iteracion, en caso que no existan 
 * datos y el promedio es cero, se captura esta exceptions
 * @return retorna un double promedio
 * @author Grupo 7
 * 
 */
    
    public double tiempoIngresoResidente() {
        double promedio;
        int contador = 0;
        long total = 0;
        for (Ingreso i : ingresos) {
            if (i.getTipo().equals("residente")) {
                long tiempo = i.getSegundos();
                total += tiempo;
                contador += 1;
            }
        }
        System.out.println(total);
        System.out.println(contador);
        try{
        promedio = (total / contador);}
        catch(Exception e){
        promedio = 0;
        }
        return promedio;
    }
    
    public void reporteCompleto(){
        final String[] Residente = {"INGRESO DE RESIDENTES:"};
        final String[] encabezadoResidente = {"Fecha de ingreso", "Tipo", "nombre del residente", "mz", "villa", "número de matrícula" };
            final Character delimiter = ';';//Delimitador, por defecto es ","
            String archivoCSV = "C:\\Users\\Luis_\\OneDrive\\Documents\\ReporteDeVisita.csv";//Nombre de archivo CSV
            ArrayList<String[]> lineas = new ArrayList<String[]>();
            lineas.add(Residente);
            lineas.add(encabezadoResidente);
            for(Ingreso i: ingresos){
                if (i.getTipo().equals("residente")){
                lineas.add(i.getLinea());
                }
            }
            
            final String[] Visitante = {"INGRESO DE VISITANTES:"};
            final String[] encabezadovisitante = {"Fecha de ingreso", "Tipo", "nombre del residente", "mz", "villa", "nombre visitante" };
            lineas.add(Visitante);
            lineas.add(encabezadovisitante);
            for(Ingreso i: ingresos){
                if (i.getTipo().equals("visitante")){
                lineas.add(i.getLinea());
                }
            }
            
          
            
        try {    
            CSVWriter writer = new CSVWriter(new FileWriter(archivoCSV));
            
            writer.writeAll(lineas);
            
            writer.close();
            System.out.println("archivo generado");
        } catch (IOException ex) {
            System.out.println("Fallo al generar archivo");
        }
    
    }
    
    public void reporteFiltroTipo(String tipo){
    if(tipo.equals("residente")){
         final String[] Residente = {"INGRESO DE RESIDENTES:"};
        final String[] encabezadoResidente = {"Fecha de ingreso", "Tipo", "nombre del residente", "mz", "villa", "número de matrícula" };
            final Character delimiter = ';';//Delimitador, por defecto es ","
            String archivoCSV = "C:\\Users\\Luis_\\OneDrive\\Documents\\ReporteDeVisita.csv";//Nombre de archivo CSV
            ArrayList<String[]> lineas = new ArrayList<String[]>();
            lineas.add(Residente);
            lineas.add(encabezadoResidente);
            for(Ingreso i: ingresos){
                if (i.getTipo().equals("residente")){
                lineas.add(i.getLinea());
                }
            }
            try {    
            CSVWriter writer = new CSVWriter(new FileWriter(archivoCSV));
            
            writer.writeAll(lineas);
            
            writer.close();
            System.out.println("archivo generado");
        } catch (IOException ex) {
            System.out.println("Fallo al generar archivo");
        }
            
            
            
    }
    
    if(tipo.equals("visitante")){
        
        
        final String[] Visitante = {"INGRESO DE VISITANTES:"};
            final String[] encabezadovisitante = {"Fecha de ingreso", "Tipo", "nombre del residente", "mz", "villa", "nombre visitante" };
            final Character delimiter = ';';//Delimitador, por defecto es ","
            String archivoCSV = "C:\\Users\\Luis_\\OneDrive\\Documents\\ReporteDeVisita.csv";//Nombre de archivo CSV
            ArrayList<String[]> lineas = new ArrayList<String[]>();
            lineas.add(Visitante);
            lineas.add(encabezadovisitante);
            for(Ingreso i: ingresos){
                if (i.getTipo().equals("visitante")){
                lineas.add(i.getLinea());
                }
            }
            
          
            
        try {    
            CSVWriter writer = new CSVWriter(new FileWriter(archivoCSV));
            
            writer.writeAll(lineas);
            
            writer.close();
            System.out.println("archivo generado");
        } catch (IOException ex) {
            System.out.println("Fallo al generar archivo");
        }
    
    
    
    
    
    
    }
    
    }
    
   
    
    
    
    
    
    
    
    }
    