/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inicio;

import java.nio.charset.Charset;
import java.util.Random;

/**
 *Class Usuario
 * Descripcion : Es una de las clases principales, de esta clase se van heredar
 * las clases residente, administrador ciudadela, administrador del sistema
 * @author gRUPO 7
 */
public class  Usuario {
    //atributos
    private String username;
    private String password;
    protected int nivel;
   
    //metodos

    public Usuario(String username, String password, int nivel){
        this.username = username;
        this.password = password;
        this.nivel = nivel;
    }
    
    

    public Usuario(int nivel) {
        this.nivel = nivel;
    }
    
    public Usuario() {
    }
    
    
    public int getNivel() {
        return nivel;
    }
    
    
        
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
    
}
